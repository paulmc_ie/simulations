var cards =
	[{
		"id": 0,
		"name": "spades",
		"available-cards": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		"used-cards": []
	},{
		"id": 1,
		"name": "clubs",
		"available-cards": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		"used-cards": []
	},{
		"id": 2,
		"name": "hearts",
		"available-cards": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		"used-cards": []
	}, {
		"id": 3,
		"name": "diamonds",
		"available-cards": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		"used-cards": []
	}];

var deck = [];
var computer_hand = [], player_hand = [];
var computer_value = 0, player_value = 0;
var deck_position = 0;
var count = 1E2;
var loop = count;

shuffleDeck();
initialDeal();
resetCards();

console.log("Computer: ", getComputerVal());
console.log("Player: ", getPlayerVal());

function shuffleDeck() {
	var suit = Math.floor(Math.random() * 4);
	var value = Math.floor((Math.random() * 14) + 1);
	if (cards[suit]["available-cards"].indexOf(value) !== -1 && cards[suit]["used-cards"].indexOf(value) === -1) {
		deck.push([suit, value]);
		cards[suit]["used-cards"].push(value);
	}
	if (deck.length < 52) {
		shuffleDeck();
	}
}

function resetCards() {
	for (var i = 0; i < 4; i++)	{
		cards[i]["used-cards"] = [];
	}
}

function initialDeal() {
	player_hand.push(deck[deck_position++]);
	computer_hand.push(deck[deck_position++]);
	player_hand.push(deck[deck_position++]);
	computer_hand.push(deck[deck_position++]);
}

function dealOne(type) {
	if (type === 0)	{
		computer_hand.push(deck[deck_position++]);
	} else {
		player_hand.push(deck[deck_position++]);
	}
}

function getPlayerVal() {
	var v = 0;
	player_hand.forEach(function(card) {
		if (card[1] > 10) {
			v += 10;
		} else {
			v += card[1];
		}
	});	
	return v;
}

function getComputerVal() {
	var v = 0;
	computer_hand.forEach(function(card) {
		if (card[1] > 10) {
			v += 10;
		} else {
			v += card[1];
		}
	});
	return v;
}

function nextPlay(type) {
	var val = 0;
	if (type === 0) {
		val = getComputerVal();
	} else {
		val = getPlayerVal();
	}
	
	if (val < 11) {
		dealOne(type);
		nextPlay(type);
	} else if (val > 11 && val < 15) {
		var r = Math.random();
		if (r <= 0.5) {
			dealOne(type);
			nextPlay(type);
		}
	} else if (val >= 15 && val <=17) {
		var r = Math.random();
		if (r < 0.25) {
			dealOne(type);
			nextPlay(type);
		}
	} else {
		return -1;
	}
}