var computer = 0, player = 0;
var wins = 0, losses = 0;
var cash = 1000000;
var bet = 100;
var max = -1000000000;
var min = 1000000000;
var reds = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
var count = 1E9;
var loop = count;

console.log("");
console.log("Simulating " + count + " spins of a roulette wheel.");
console.log("Betting on single numbers only. Return is 35 to 1.");
console.log("Please wait...");

var d = new Date();
var start = d.getTime();

while (loop > 0) {
	computer = Math.floor(Math.random() * 37);
	player = Math.floor(Math.random() * 37);
	
	if (computer === player) {
		cash += (bet * 36);
		wins++;
	} else {
		cash -= bet;
		losses++;
	}
	
	cash > max ? max = cash : 0;
	cash < min ? min = cash : 0;
	
	loop--;
}

d = new Date();
var end = d.getTime();
console.log("");
console.log("Finished simulation.");
console.log("");
console.log("Time Taken: " + ((end - start) / 1000) + " seconds.");
console.log("");
console.log("Results:");
console.log("Wins:\t\t" + wins + " = " + (wins * 100 / count).toFixed(4) + "%"); 
console.log("Losses:\t\t" + losses + " = " + (losses * 100 / count).toFixed(4) + "%");
console.log("Cash:\t\t" + cash);
console.log("High:\t\t" + max);
console.log("Low:\t\t" + min);
console.log("Total:\t\t" + count);