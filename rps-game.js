var player, computer;
var wins = 0, losses = 0, draws = 0;
var result = 0;
var plays = ["ROCK", "PAPER", "SCISSORS"];
var readline = require("readline");
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

console.log("Welcome to Rock [r], Paper [p], Scissors [s]! Quit [q]");
console.log("Won: " + wins + " Lost: " + losses + " Drew: " + draws);
rl.setPrompt("Rock, Paper, Scissors? ");
rl.prompt();
rl.on("line", function(a) {
	computer = Math.floor(Math.random() * 3);
	var ans = a.toUpperCase();

	switch(ans[0]) {
		case "R":
			player = 0;
			break;
		case "P":
			player = 1;
			break;
		case "S":
			player = 2;
			break;
		case "Q":
			rl.close();
		default:
			rl.prompt();
			break;
	}
	
	switch(computer) {
		case 0:
			if (player === 1) {
				result = 0;
			} else if (player === 0) {
				result = 1;
			} else {
				result = 2;
			}
			break;
		case 1:
			if (player === 2) {
				result = 0;
			} else if (player === 1) {
				result = 1;
			} else {
				result = 2;
			}
			break;
		case 2:
			if (player === 0) {
				result = 0;
			} else if (player === 2) {
				result = 1;
			} else {
				result = 2;
			}
			break;
	}
	
	if (result === 0) {
		console.log("Congratulations, you won! Computer picked " + plays[computer].toLowerCase() + ".");
		console.log("Won: " + ++wins + " Lost: " + losses + " Drew: " + draws);
		rl.prompt();
	} else if (result === 2) {
		console.log("Hard luck, computer picked " + plays[computer].toLowerCase() + ".");
		console.log("Won: " + wins + " Lost: " + ++losses + " Drew: " + draws);
		rl.prompt();
	} else if (result === 1) {
		console.log("Draw! Computer also picked " + plays[computer].toLowerCase() + ".");
		console.log("Won: " + wins + " Lost: " + losses + " Drew: " + ++draws);
		rl.prompt();
	}
}).on("close", function() {
	process.exit(0);
});
