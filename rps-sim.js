var computer, player;
var count = 1E9;
var wins = 0, draws = 0, losses = 0;
var loop = count;
console.log("");
console.log("Simulating " + count + " games of Rock, Paper, Scissors.");
console.log("Please wait...");
var d = new Date();
var start = d.getTime();

while (loop > 0) {
	computer = Math.floor(Math.random() * 3);
	player = Math.floor(Math.random() * 3);
	
	switch(computer) {
		case 0:
			if (player === 1) {
				wins++;
			} else if (player === 0) {
				draws++;
			} else {
				losses++;
			}
			break;
		case 1:
			if (player === 2) {
				wins++;
			} else if (player === 1) {
				draws++;
			} else {
				losses++;
			}
			break;
		case 2:
			if (player === 0) {
				wins++;
			} else if (player === 2) {
				draws++;
			} else {
				losses++;
			}
			break;
	}
	loop--;	
}
d = new Date();
var end = d.getTime();
console.log("");
console.log("Finished simulation.");
console.log("");
console.log("Time Taken: " + ((end - start) / 1000) + " seconds.");
console.log("");
console.log("Results:");
console.log("Wins:\t\t" + wins + " = " + (wins * 100 / count).toFixed(4) + "%"); 
console.log("Draws:\t\t" + draws + " = " + (draws * 100 / count).toFixed(4) + "%"); 
console.log("Losses:\t\t" + losses + " = " + (losses * 100 / count).toFixed(4) + "%"); 
console.log("Total:\t\t" + count);